package com.example.objectdbdemo.service;

import com.example.objectdbdemo.model.entity.Point;
import com.example.objectdbdemo.model.request.CreatePointRequest;
import com.example.objectdbdemo.repository.PointRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class PointService {

  private PointRepository pointRepository;

  public PointService(PointRepository pointRepository) {
    this.pointRepository = pointRepository;
  }

  public List<Point> getAll() {
    return pointRepository.findAll();
  }

  public Point create(CreatePointRequest request) {
    Point point = new Point(request.getX(), request.getY());

    return pointRepository.save(point);
  }
}
