package com.example.objectdbdemo.controller;

import com.example.objectdbdemo.model.request.CreatePointRequest;
import com.example.objectdbdemo.service.PointService;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/points")
public class PointController {

  private PointService pointService;

  public PointController(PointService pointService) {
    this.pointService = pointService;
  }

  @GetMapping
  public ResponseEntity getAll() {
    return ResponseEntity.ok(pointService.getAll());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody @Valid CreatePointRequest request) {
    return ResponseEntity.status(HttpStatus.CREATED).body(pointService.create(request));
  }
}
