package com.example.objectdbdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class ObjectdbdemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(ObjectdbdemoApplication.class, args);
  }

}
